//
//  ProfileTableViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/4/26.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class ProfileTableViewController: BaseViewTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
      visitorView1.setupVisitorViewInfo("visitordiscover_image_profile", tipString: "登录后，你的微博、相册、个人资料会显示在这里，展示给别人")
    }

   
}
