//
//  UIButtonExtension.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/6.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

extension UIButton{
    convenience init(imageName:String,bgimageName:String){
       self.init()
        setImage(UIImage.init(named: imageName), forState: .Normal)
        setImage(UIImage.init(named: imageName+"_highlighted"), forState: .Highlighted)
        setBackgroundImage(UIImage.init(named: bgimageName), forState: .Normal)
        setBackgroundImage(UIImage.init(named: bgimageName+"_highlighted"), forState: .Highlighted)
        sizeToFit()
    }
   class func createButton(imageName:String,bgimgaeName:String) -> UIButton {
        let btn = UIButton.init(imageName: imageName, bgimageName: bgimgaeName)
        return btn
    }
    convenience init(title : String,backgroundColor : UIColor,fontSize : CGFloat){
        self.init()
        setTitle(title, forState: .Normal)
       self.backgroundColor = backgroundColor
        titleLabel?.font = UIFont.systemFontOfSize(fontSize)
    }
}
