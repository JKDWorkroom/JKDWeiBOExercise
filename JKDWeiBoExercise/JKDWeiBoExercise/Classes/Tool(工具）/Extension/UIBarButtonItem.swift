//
//  UIBarButtonItem.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/8.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
extension UIBarButtonItem{
    convenience  init(imageName:String,target : AnyObject,action: Selector) {
        let btn = UIButton(type: .Custom)
        btn.setImage(UIImage.init(named: imageName), forState: .Normal)
        btn.setImage(UIImage.init(named: imageName + "_highlighted"), forState: .Highlighted)
        btn.sizeToFit()
        btn.addTarget(target, action: action, forControlEvents: .TouchUpInside)
        self.init(customView:btn)
    }
}
