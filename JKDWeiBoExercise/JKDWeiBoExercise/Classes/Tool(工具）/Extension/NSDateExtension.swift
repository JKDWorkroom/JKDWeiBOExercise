//
//  File.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/31.
//  Copyright © 2016年 JKD. All rights reserved.
//

import Foundation
extension NSDate {
    class func createTimeString(createAtString : String) -> String {
//        1.创建一个格式对象
        let fmt = NSDateFormatter()
//        2.设置格式对象格式
        fmt.dateFormat = "EEE MM dd HH:mm:ss Z yyyy"
        fmt.locale = NSLocale(localeIdentifier: "en")
//        3.将字符串转成时间
        guard let createDate = fmt.dateFromString(createAtString) else{
            return""
        }
//        4.获取当前时间
        let nowDate = NSDate()
//        5.或缺当前时间与创建时间只差
        let intervalTime = Int(nowDate.timeIntervalSinceDate(createDate))
//        一分钟之内
        if intervalTime < 60 {
            return "刚刚"
        }
//        一小时之内
        if intervalTime < 60 * 60 {
            return "\(intervalTime/60)分钟前"
        }
//        一天之内
        if intervalTime < 60 * 60 * 24{
            return "\(intervalTime/60/60)小时之前"
        }
//        6.创建日历对象
        let calendar = NSCalendar.currentCalendar()
//        判断是否为昨天
        if calendar.isDateInYesterday(createDate) {
            fmt.dateFormat = "HH:mm"
            let timeString = fmt.stringFromDate(createDate)
            return "昨天\(timeString)"
        }
//        判断是否为一年之内
        let cmps = calendar.components(.Year, fromDate: createDate, toDate: nowDate, options: [])
        if cmps.year < 1{
            fmt.dateFormat = "MM-dd HH:mm"
            let timeString = fmt.stringFromDate(createDate)
            return timeString
        }
//        大于一年

            fmt.dateFormat = "yyyy-MM-dd HH:mm"
            let timeString = fmt.stringFromDate(createDate)
            return timeString
        
    }
}