//
//  Common.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/14.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
//授权常量
let app_key = "816480731"
let app_secret = "2a7f1bdc040b8a2c0181e0c0d0800942"
let redirect_uri = "http://www.baidu.com"

//页面边距
let edgeMargin : CGFloat = 15
let padding : CGFloat = 10

//通知常量
let showPhotoBrowserVcNote = "showPhotoBrowserVcNote"
let showPhotoBrowserVcNoteIndex = "showPhotoBrowserVcNoteIndex"
let showPhotoBrowserVcNotePicURLs = "showPhotoBrowserVcNotePicURLs"