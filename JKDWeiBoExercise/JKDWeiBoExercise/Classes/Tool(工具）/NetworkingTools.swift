//
//  NetworkingTools.swift
//  AFNetWorkingTools
//
//  Created by 简康达 on 16/5/13.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
import AFNetworking
enum MethodType {
    case GET
    case POST
}
class NetworkTools: AFHTTPSessionManager {
    // 将工具类设计成单例对象
    static let shareIntance : NetworkTools = {
        let tools = NetworkTools()
        tools.responseSerializer.acceptableContentTypes?.insert("text/plain")
        return tools
    }()
}
//MARK：网络请求工具
extension NetworkTools{
    func request(Met :MethodType,URLString:String,parameters:[String:AnyObject],finished:(result:AnyObject?,error:NSError?)->()){
//        定义成功回调
        let successCellBack = { (task:NSURLSessionDataTask,result:AnyObject?)in
            finished(result: result, error: nil)
        }
//        定义失败回调
        let failureCellBack = { (task:NSURLSessionDataTask?,error:NSError)in
            finished(result: nil, error: error)
        }
        
        if Met == .GET{
            GET(URLString, parameters: parameters, success: successCellBack, failure: failureCellBack)}else{
        POST(URLString,parameters : parameters,progress : { (nil) in
            },success : successCellBack,failure : failureCellBack)}
    }
}
//MARK: 再次封装网络请求
extension NetworkTools{
    func loadDataThroughInternet(methodType:MethodType = .POST,urlString:String,parameters:[String:AnyObject],finished:(result:AnyObject?,error:NSError?)->()){
        request(methodType, URLString: urlString, parameters: parameters, finished: finished)
    }
}
extension NetworkTools{
    func loadStatuses(methodType:MethodType = .GET,urlString:String,parameters:[String:AnyObject],finished:(result:[[String:AnyObject]]?,error:NSError?)->()) {
        request(methodType, URLString: urlString, parameters: parameters) { (result, error) in
            guard let resultDict = result as? [String:AnyObject] else{
                finished(result:nil,error:error)
                return
            }
            finished(result: resultDict["statuses"] as? [[String:AnyObject]], error:nil)
        }
    }
}
extension NetworkTools{
    func composeStatus(methodType:MethodType = .POST,text:String,image:UIImage?,finished:(isSucess:Bool)->()) {
        let parameters :[String:AnyObject] = ["access_token":(UserAccountViewModel.shareIntance.account?.access_token)!,"status":text]
        if image == nil{
            let urlString = "https://api.weibo.com/2/statuses/update.json"
            request(methodType, URLString: urlString, parameters: parameters, finished: { (result, error) in
                if result != nil {
                    finished (isSucess: true)
                }else{
                    finished(isSucess: false)
                }
            })
        }else{
            let urlString = "https://api.weibo.com/2/statuses/upload.json"
            POST(urlString, parameters:parameters, constructingBodyWithBlock: { (formdata) in
                let imageData = UIImageJPEGRepresentation(image!, 0.4)
                formdata.appendPartWithFileData(imageData!, name: "pic", fileName: "11", mimeType: "image/png")
                }, progress: nil, success: { (_, result) in
                    if result != nil{
                     finished(isSucess: true)
                    }
                }, failure: { (_, error) in
                    finished(isSucess: false)
                    print(error)
            })
        }
    }
}