//
//  PackageManager.swift
//  表情键盘的搭建
//
//  Created by 简康达 on 16/6/17.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class PackageManager {
//    MAEK:- 属性
    var packages : [EmoticonPackage] = [EmoticonPackage]()
//    重写构造函数
    init(){
        // 1.添加最近的包
        packages.append(EmoticonPackage(id: ""))
        
        // 2.添加默认包
        packages.append(EmoticonPackage(id: "com.sina.default"))
        
        // 3.添加emoji包
        packages.append(EmoticonPackage(id: "com.apple.emoji"))
        
        // 4.添加浪小花包
        packages.append(EmoticonPackage(id: "com.sina.lxh"))
    }
}
