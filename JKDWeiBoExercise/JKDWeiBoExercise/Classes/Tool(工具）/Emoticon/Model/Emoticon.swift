//
//  Emoticon.swift
//  表情键盘的搭建
//
//  Created by 简康达 on 16/6/17.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class Emoticon: NSObject {
//    MARK:- 属性
    var code : String?{
        didSet{
//            进行nil的校验
            guard let code = code else{
                return
            }
            
            // 1.创建扫描器
            let scannner = NSScanner(string: code)
            
            // 2.扫描字符串中的内容,并且获取扫描结果
            var value : UInt32 = 0
            scannner.scanHexInt(&value)
            
            // 3.将value转成字符
            let c = Character(UnicodeScalar(value))
            
            // 4.将字符转成字符串
            emoji = String(c)
        }
    }
    var chs : String?
    var png : String?{
        didSet{
//            nil值校验
            guard let png = png else{
                return
            }
            pngPath = NSBundle.mainBundle().bundlePath + "/Emoticons.bundle/" + png
        }
    }
    var emoji : String?
    var pngPath : String?
    var isRemove : Bool = false
    var isEmpty : Bool = false
//    MARK:- 构造函数
    init(isEmpty:Bool) {
        self.isEmpty = isEmpty
    }
    init(isRemove : Bool) {
        self.isRemove = isRemove
    }
    init(dict: [String: AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {}
//    重写打印方式
   override var description : String{
        return dictionaryWithValuesForKeys(["emoji","chs","pngPath"]).description
    }
}
