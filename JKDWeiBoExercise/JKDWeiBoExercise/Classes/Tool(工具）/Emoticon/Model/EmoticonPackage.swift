//
//  EmoticonPackage.swift
//  表情键盘的搭建
//
//  Created by 简康达 on 16/6/17.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class EmoticonPackage: NSObject {
//    MARK:- 属性
    var emoticon : [Emoticon] = [Emoticon]()
//    MAEK:- 重写构造函数
     init(id : String) {
        super.init()
        if id == "" {
            addEmptyEmoticon()
            return
        }
//        获取info.plist文件路径
        guard let infoPath = NSBundle.mainBundle().pathForResource("\(id)/info.plist", ofType: nil, inDirectory: "Emoticons.bundle") else{
            return
        }
//        获取info.plist文件的数据
        guard let infoData = NSArray(contentsOfFile: infoPath) as? [[String : AnyObject]] else{
            return
        }
//        遍历数组
        var index = 0
        for var item in infoData {
            index += 1
//            将所有item绑定一个id路径
            if let png = item["png"] as? String{
                item["png"] = "\(id)/" + png
            }
            emoticon.append(Emoticon(dict: item))
            if index % 20 == 0 {
                emoticon.append(Emoticon(isRemove: true))
            }
        }
        addEmptyEmoticon()
            }
//   添加空白按钮
    func addEmptyEmoticon(){
        let leftCount = emoticon.count % 21
        if leftCount != 0 || emoticon.count == 0 {
            for _ in leftCount..<20 {
                emoticon.append(Emoticon(isEmpty: true))
            }
            //            添加删除按钮
            emoticon.append(Emoticon(isRemove: true))
        }

    }
}
