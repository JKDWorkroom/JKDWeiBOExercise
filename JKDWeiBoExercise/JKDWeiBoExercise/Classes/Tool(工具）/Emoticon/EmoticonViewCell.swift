//
//  EmoticonViewCell.swift
//  表情键盘的搭建
//
//  Created by 简康达 on 16/6/17.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class EmoticonViewCell: UICollectionViewCell {
//    懒加载属性
    private lazy var emoticonBtn = UIButton()
//    属性
    var emoticon : Emoticon?{
        didSet{
            guard let emoticon = emoticon else{
                return
            }
            emoticonBtn.setImage(UIImage.init(contentsOfFile: emoticon.pngPath ?? ""), forState: .Normal)
            emoticonBtn.setTitle(emoticon.emoji, forState: .Normal)
            if emoticon.isRemove {
                emoticonBtn.setImage(UIImage(named:"compose_emotion_delete"), forState: .Normal)
            }
            
        }
    }
    
//    MARK:- 构造函数
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
}
//UI相关设置
extension EmoticonViewCell{
    func setupUI() {
//        添加子控件
        contentView.addSubview(emoticonBtn)
//        设置frame 
        emoticonBtn.frame = CGRectInset(contentView.frame, 5, 5)
//        设置emoji表情显示的大小
        emoticonBtn.titleLabel?.font = UIFont.systemFontOfSize(32)
//        让按钮不能处理事件
        emoticonBtn.userInteractionEnabled = false
    }
}
