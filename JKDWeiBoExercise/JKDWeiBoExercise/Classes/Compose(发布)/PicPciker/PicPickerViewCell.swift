//
//  PicPickerViewCell.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/14.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
/* 在swift中写协议,并且协议中有可选方法
 1> 在方法前加载 optional
 2> 在protocol上方必须@objc
 3> 在调用可选方法时需要先判断是否有实现,之后再调用时需要对方法进行强制解包
 */
protocol PicPickerViewCellDelegate : NSObjectProtocol {
    func addPhotoBtnClickFor(cell : UICollectionViewCell)
    func deletePhotoBtnClickFor(cell : PicPickerViewCell)
}
class PicPickerViewCell: UICollectionViewCell {
    @IBOutlet weak var addPhotoBtn: UIButton!
    @IBOutlet weak var deletePhotoBtn: UIButton!
    var delegate : PicPickerViewCellDelegate?
    var image : UIImage? {
        didSet{
            if image != nil{
                addPhotoBtn.setBackgroundImage(image, forState: .Normal)
                addPhotoBtn.userInteractionEnabled = false
                deletePhotoBtn.hidden = false
            }else{
                addPhotoBtn.setBackgroundImage(UIImage(named: "compose_pic_add"), forState: .Normal)
                addPhotoBtn.userInteractionEnabled = true
                deletePhotoBtn.hidden = true
            }
        }
    }
    @IBAction func addPhotoBtnClick() {
        delegate?.addPhotoBtnClickFor(self)
    }
    @IBAction func deletePhotoBtnClick() {
        delegate?.deletePhotoBtnClickFor(self)
    }
}
