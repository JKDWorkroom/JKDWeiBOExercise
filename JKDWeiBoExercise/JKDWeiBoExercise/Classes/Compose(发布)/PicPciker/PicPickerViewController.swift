//
//  PicPickerCollectionViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/14.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class PicPickerViewController: UICollectionViewController {
//MARK:- 懒加载属性
    lazy var images = [UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView?.registerNib(UINib(nibName: "PicPickerViewCell",bundle: nil), forCellWithReuseIdentifier: reuseIdentifier)
    }
}
extension PicPickerViewController{
    
    override func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return images.count + 1
    }
    
    override func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(reuseIdentifier, forIndexPath: indexPath) as! PicPickerViewCell
//        给cell设置数据
        cell.backgroundColor = UIColor.redColor()
        cell.delegate = self
        cell.image = indexPath.row < images.count ? images[indexPath.row] : nil
        return cell
    }

}
//MARK:- 遵守协议
extension PicPickerViewController : PicPickerViewCellDelegate{
    func addPhotoBtnClickFor(cell: UICollectionViewCell) {
//        判断照片源是否可用
        if !UIImagePickerController.isSourceTypeAvailable(.PhotoLibrary){
            return
        }
//        创建照片选择控制器
        let imagePickerController = UIImagePickerController()
//        设置代理
        imagePickerController.delegate = self
//        弹出控制器
        presentViewController(imagePickerController, animated: true, completion: nil)
    }
    func deletePhotoBtnClickFor(cell: PicPickerViewCell) {
//        print(cell.images)
        let index = collectionView?.indexPathForCell(cell)?.item
        images.removeAtIndex(index!)
        collectionView?.reloadData()
    }
    
}
//MARK:- 遵守UIImagePickerController的协议
extension PicPickerViewController : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : AnyObject]) {
        guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else{
            picker.dismissViewControllerAnimated(true, completion: nil)
            return
        }
        images.append(image)
        self.collectionView?.reloadData()
        picker.dismissViewControllerAnimated(true, completion: nil)
    }
}
//MARK:- 自定义CollectionView的layout
class PicPickerCollectionViewLayout : UICollectionViewFlowLayout{
    override func prepareLayout() {
        super.prepareLayout()
//        定义间距
        let itemPadding : CGFloat = 15
        let itemWH = (UIScreen.mainScreen().bounds.width - 4 * itemPadding) / 3
//        设置布局相关属性
        itemSize = CGSize(width: itemWH, height: itemWH)
        minimumInteritemSpacing = itemPadding
        minimumLineSpacing = itemPadding
//        设置CollectionView属性
        collectionView?.contentInset = UIEdgeInsetsMake(itemPadding, itemPadding, itemPadding, itemPadding)
    }
}
