//
//  ComposeTitleView.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/12.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
import SnapKit
class ComposeTitleView: UIView {
//懒加载属性
    private lazy var postStatusLabel : UILabel = UILabel()
    private lazy var screenNameLabel : UILabel = UILabel()
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }

}
//MARK:- UI相关设置
extension ComposeTitleView {
    private func setupUI(){
//        1.添加子控件
        addSubview(postStatusLabel)
        addSubview(screenNameLabel)
//        2.设置frame
        postStatusLabel.snp_makeConstraints { (make) in
            make.centerX.equalTo(self.snp_centerX)
            make.top.equalTo(self.snp_top)
        }
        screenNameLabel.snp_makeConstraints { (make) in
            make.top.equalTo(postStatusLabel.snp_bottom).offset(3)
            make.centerX.equalTo(postStatusLabel.snp_centerX)
        }
//        3.设置属性
        postStatusLabel.text = "发微博"
        postStatusLabel.font = UIFont.systemFontOfSize(14)
        screenNameLabel.text = UserAccountViewModel.shareIntance.account?.screen_name
        screenNameLabel.font = UIFont.systemFontOfSize(13)
        screenNameLabel.textColor = UIColor.lightGrayColor()
    }
}