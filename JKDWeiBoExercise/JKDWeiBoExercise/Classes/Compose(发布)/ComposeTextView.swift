//
//  ComposeTextView.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/13.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
import SnapKit
class ComposeTextView: UITextView {
    
//    MARK:- 懒加载属性
    lazy var placeHolderLabel = UILabel()
//    MARK:- 构造函数
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupUI()
    }
}
//   MARK:- UI相关设置
extension ComposeTextView{
    private func setupUI(){
//   将placeHolderLabel添加到ComposeTextView中
        addSubview(placeHolderLabel)
//   设置placeHolderLabel的属性
        placeHolderLabel.text = "分享新鲜事..."
        placeHolderLabel.font = font
        placeHolderLabel.textColor = UIColor.lightGrayColor()
//   给placeHolderLabel添加约束
        placeHolderLabel.snp_makeConstraints { (make) in
            make.top.equalTo(8)
            make.leading.equalTo(10)
        }
//   调整输入框光标位置
        self.textContainerInset = UIEdgeInsetsMake(8, 8, 0, 8)
    }
}