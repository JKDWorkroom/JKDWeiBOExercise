//
//  VisitorView.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/8.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class VisitorView: UIView {
//MARK:通过xib快速创建view
   class func visitorView() -> VisitorView {
        return NSBundle.mainBundle().loadNibNamed("VisitorView", owner: nil, options: nil).first as! VisitorView
    }
    @IBOutlet weak var rotationView: UIImageView!
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var tipLabel: UILabel!
    @IBOutlet weak var registerBtn: UIButton!
    @IBOutlet weak var loginBtn: UIButton!
    func setupVisitorViewInfo(iconName:String,tipString:String){
        rotationView.hidden = true
        iconView.image = UIImage.init(named: iconName)
        tipLabel.text = tipString
    }
    func addRotationAnimate(){
//        1.创建动画
        let rotationAmimate = CABasicAnimation(keyPath: "transform.rotation.z")
//        2.设置动画属性
        rotationAmimate.fromValue = 0
        rotationAmimate.toValue = M_PI * 2
        rotationAmimate.duration = 10
        rotationAmimate.repeatCount = MAXFLOAT
        // 将该属性设置成false,那么view消失时,动画不会被移除,即在切换页面后，再回到原页面，动画依然在。
        rotationAmimate.removedOnCompletion = false
//        3.将动画添加到图层
        rotationView.layer.addAnimation(rotationAmimate, forKey: nil)
        
    }
    
}

