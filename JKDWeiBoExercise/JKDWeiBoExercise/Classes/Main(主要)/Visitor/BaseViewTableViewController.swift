//
//  BaseViewTableViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/8.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class BaseViewTableViewController: UITableViewController {
    
     var isLogin = UserAccountViewModel.shareIntance.isLogin
     var visitorView1 : VisitorView = VisitorView.visitorView()
    override func loadView() {
        isLogin ? super.loadView() : setupView()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    func setupView() {
//        visitorView1.backgroundColor = UIColor.redColor()
        view = visitorView1
//        1.设置导航栏的item
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "注册", style: .Plain, target: self, action: #selector(BaseViewTableViewController.registerBtnClick))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "登录", style: .Plain, target:self, action: #selector(BaseViewTableViewController.loginBtnClick))
        visitorView1.registerBtn.addTarget(self, action: #selector(BaseViewTableViewController.registerBtnClick), forControlEvents: .TouchUpInside)
        visitorView1.loginBtn.addTarget(self, action: #selector(BaseViewTableViewController.loginBtnClick), forControlEvents: .TouchUpInside)
    
    }
}
extension BaseViewTableViewController{
   @objc func registerBtnClick(){
        print("点击了注册")
    }
   @objc func loginBtnClick(){
    let oauthVC = OAuthViewController()
    let oauthNav = UINavigationController(rootViewController: oauthVC)
    presentViewController(oauthNav, animated: true, completion: nil)
    
    }
}

