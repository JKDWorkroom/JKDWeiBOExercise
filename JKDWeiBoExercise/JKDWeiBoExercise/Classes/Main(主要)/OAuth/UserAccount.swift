//
//  UserAccount.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/15.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class UserAccount: NSObject,NSCoding{
    var access_token :String?
    var expires_in : NSTimeInterval = 0
        {
//        属性监听器，当该属性发现变化，就会调用
        didSet{
            expires_date = NSDate(timeIntervalSinceNow: expires_in)
        }
    }
    var uid : String?
    var avatar_large : String?
    var screen_name :String?
    var expires_date : NSDate?
    init(dict :[String:AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
    override var description: String{
        get{
            return dictionaryWithValuesForKeys(["access_token", "expires_in", "uid","avatar_large","screen_name","expires_date"]).description
        }
    }
    
//    归档&解档
    func encodeWithCoder(aCoder: NSCoder) {
        aCoder.encodeObject(access_token, forKey: "access_token")
        aCoder.encodeObject(avatar_large, forKey: "avatar_large")
        aCoder.encodeObject(screen_name, forKey: "screen_name")
        aCoder.encodeObject(expires_date, forKey: "expires_date")
        aCoder.encodeObject(uid, forKey: "uid")
    }
    required init?(coder aDecoder: NSCoder) {
        access_token = aDecoder.decodeObjectForKey("access_token") as? String
        avatar_large = aDecoder.decodeObjectForKey("avatar_large") as? String
        screen_name = aDecoder.decodeObjectForKey("screen_name") as? String
        expires_date = aDecoder.decodeObjectForKey("expires_date") as? NSDate
        uid = aDecoder.decodeObjectForKey("uid") as? String
    }
}

