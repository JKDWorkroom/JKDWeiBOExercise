//
//  MainViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/4/26.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class MainViewController: UITabBarController {

    private lazy var btn = UIButton(imageName: "tabbar_compose_icon_add", bgimageName: "tabbar_compose_button")
//    private lazy var btn = UIButton.createButton("tabbar_compose_icon_add", bgimgaeName: "tabbar_compose_button")
    private lazy var imageArray = ["tabbar_home", "tabbar_message_center", "", "tabbar_discover", "tabbar_profile"]
    override func viewDidLoad() {
        super.viewDidLoad()
        setupButton()
        
    }
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
       adjustItems()
    }


}
extension MainViewController{
    private func setupButton(){
        tabBar.addSubview(btn)
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add"), forState:.Normal)
//        btn.setImage(UIImage(named: "tabbar_compose_icon_add_highlighted"), forState: .Highlighted)
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button"), forState: .Normal)
//        btn.setBackgroundImage(UIImage(named: "tabbar_compose_button_highlighted"), forState: .Highlighted)
//        btn.sizeToFit()
        btn.center = CGPoint(x: tabBar.bounds.width * 0.5, y: tabBar.bounds.height * 0.5)
        // 监听按钮的点击
        // Selector包装:1.Selector("composeBtnClick") 2."composeBtnClick"
        // OC中监听事件本质:给某一个对象发送消息. 发送消息的本质:去对象消息列表中寻找SEL
        // 在swift中,如果将一个函数声明称private,那么该函数不会被添加到消息列表中
        // 如果在私有函数前加@objc,那么该函数依然是私有,并且会被添加到消息列表中
        btn.addTarget(self, action:#selector(MainViewController.composeClick), forControlEvents: .TouchUpInside)
    }
    private func adjustItems(){
        for i in 0..<tabBar.items!.count {
            if i==2 {
                tabBar.items![i].enabled = false
            }
            tabBar.items![i].selectedImage = UIImage(named: imageArray[i] + "_highlighted")
        }

    }
}

extension MainViewController{
   @objc private func composeClick(){
//        1.创建控制器
    let composeVC = ComposeViewController()
//        2.包装一个导航栏控制器
    let composeNav = UINavigationController.init(rootViewController: composeVC)
//        3.弹出控制器
    presentViewController(composeNav, animated: true, completion: nil)
    }
}
