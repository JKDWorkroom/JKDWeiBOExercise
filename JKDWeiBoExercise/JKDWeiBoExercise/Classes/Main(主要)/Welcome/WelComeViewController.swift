//
//  WelComeViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/21.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
import SDWebImage
class WelComeViewController: UIViewController {

    @IBOutlet weak var iconImageViewTopCons: NSLayoutConstraint!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        iconImageView.layer.cornerRadius = 42.5
        iconImageView.layer.masksToBounds = true
        let url = NSURL(string: UserAccountViewModel.shareIntance.account?.avatar_large ?? "")
        iconImageView.setImageWithURL(url!)
        label.text = UserAccountViewModel.shareIntance.account?.screen_name ?? "欢迎回来"
        iconImageViewTopCons.constant = -100
        UIView.animateWithDuration(1.5, delay: 0, usingSpringWithDamping: 0.6, initialSpringVelocity: 5.0, options: [], animations: {
            self.view.layoutIfNeeded()
            }) { (_) in
                UIApplication.sharedApplication().keyWindow?.rootViewController = UIStoryboard(name: "Main", bundle: nil).instantiateInitialViewController()!
        }
    }
    

}
