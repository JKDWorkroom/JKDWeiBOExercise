//
//  DiscoverTableViewController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/4/26.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class DiscoverTableViewController: BaseViewTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        visitorView1.setupVisitorViewInfo("visitordiscover_image_message", tipString: "登录后，别人评论你的微博，给你发消息，都会在这里收到通知")
    }

}
