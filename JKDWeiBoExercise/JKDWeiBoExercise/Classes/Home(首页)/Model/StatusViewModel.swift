//
//  StatusViewModel.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/31.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class StatusViewModel {
    var status : Status?
// MARK:- 对微博数据进行处理
    var sourceText :String?
    ///    认证图标
    var verifiedImage : UIImage?
    ///    vip图标
    var vipImage : UIImage?
///    对创建时间进行处理
    var createAtString : String? {
        guard let createAtString = status?.created_at else{
            return ""
        }
        return NSDate.createTimeString(createAtString)
    }
   /// 所有的配图对应的NSURL数组
    var picURLs : [NSURL] = [NSURL]()
    
//    自定义构造函数
    init(status:Status){
        //            1.判断来源是否有值
        guard let source = status.source where status.source != "" else{
            return
        }
        //            2.截取字符串
        let startIndex = (source as NSString).rangeOfString(">").location+1
        let length = (source as NSString).rangeOfString("</").location - startIndex
        sourceText = "来自"+(source as NSString).substringWithRange(NSRange(location: startIndex, length: length))
        self.status = status
//        设置认证图片
        let type = status.user?.verified_type ?? -1
        switch type {
        case 0:
            verifiedImage = UIImage(named: "avatar_vip")
        case 2,3,5:
            verifiedImage = UIImage(named: "avatar_enterprise_vip")
        case 220:
            verifiedImage = UIImage(named: "avatar_grassroot")
        default:
            verifiedImage = nil
        }
//        对图片数据的处理
        if let picURLDicts = status.pic_urls?.count != 0 ? status.pic_urls : status.retweeted_status?.pic_urls {
            for picURLDict in picURLDicts {
                let urlString = picURLDict["thumbnail_pic"] as? String
                let picURL = NSURL(string: urlString ?? "" )
                picURLs.append(picURL!)
            }
        }
//        设置会员图片
        let mbrank = status.user?.mbrank ?? 0
            if mbrank >= 1 && mbrank <= 6 {
            
                vipImage = UIImage(named: "common_icon_membership_level\(mbrank)")
            }else{
                vipImage = nil
            }
    }
    
}
