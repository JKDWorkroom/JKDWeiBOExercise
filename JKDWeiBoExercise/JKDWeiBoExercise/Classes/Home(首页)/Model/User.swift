//
//  User.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/31.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class User: NSObject {
//    认证类型
    var verified_type : Int = -1
//    会员等级
    var mbrank : Int = 0
//    用户头像
    var profile_image_url : String?
//   昵称
    var screen_name : String?    
    
    init(dict : [String : AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
