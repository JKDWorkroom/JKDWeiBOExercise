//
//  Status.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/30.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class Status: NSObject {
    // MARK:- 属性
    /// 创建时间
    var created_at : String?
    /// 微博的ID
    var id : Int = 0
    /// 微博正文
    var text : String?
    /// 来源<a href=\"http://app.weibo.com/t/feed/9ksdit\" rel=\"nofollow\">iPhone客户端</a>
    var source : String?
    var user : User?
//    所有配图数据
    var pic_urls : [[String:AnyObject]]?
    var retweeted_status : Status?
// MARK:自定义构造函数
    init(dict:[String:AnyObject]) {
        super.init()
        setValuesForKeysWithDictionary(dict)
        if let userDict = dict["user"] as? [String : AnyObject] {
            user = User(dict: userDict)
        }
        if let retweetedStatusDict = dict["retweeted_status"] as? [String : AnyObject]{
            retweeted_status = Status(dict: retweetedStatusDict)
        }
    }
    override func setValue(value: AnyObject?, forUndefinedKey key: String) {
    }
}
