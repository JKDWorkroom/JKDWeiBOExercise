//
//  PhotoBrowserAnimator.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/27.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
protocol PhotoBrowserPresentedDelegate :NSObjectProtocol {
    func startRectForPresentedView(indexPath:NSIndexPath) -> CGRect
    func endRectForPresentedView(indexPath:NSIndexPath) -> CGRect
    func imageViewForPresentedView(indexPath:NSIndexPath) -> UIImageView
}
protocol PhotoBrowserDismissDelegate : NSObjectProtocol {
    func imageViewForDismissView() -> UIImageView
    func indexPathForDismissView() -> NSIndexPath
}
class PhotoBrowserAnimator: NSObject {
    var isPresented:Bool = false
    var presentedDelegate : PhotoBrowserPresentedDelegate?
    var dismissDelegate: PhotoBrowserDismissDelegate?
    var indexPath : NSIndexPath?
}
extension PhotoBrowserAnimator:UIViewControllerTransitioningDelegate{
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = true
        return self
    }
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = false
        return self
    }
}
extension PhotoBrowserAnimator:UIViewControllerAnimatedTransitioning{
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 1.0
    }
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        isPresented ? animateForPresentedView(transitionContext) : animateForDismissView(transitionContext)
    }
}
extension PhotoBrowserAnimator{
    func animateForPresentedView(transitionContext:UIViewControllerContextTransitioning) {
//        nil值校验
        guard let presentedDelegate = self.presentedDelegate , indexPath = self.indexPath else{
            return
        }
//         取出将要弹出的View
        guard let presentedView = transitionContext.viewForKey(UITransitionContextToViewKey)else{
            return
        }
        presentedView.alpha = 0.0
        transitionContext.containerView()?.addSubview(presentedView)
        let imageView = presentedDelegate.imageViewForPresentedView(indexPath)
        imageView.frame = presentedDelegate.startRectForPresentedView(indexPath)
        transitionContext.containerView()?.addSubview(imageView)
        transitionContext.containerView()?.backgroundColor = UIColor.blackColor()
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            imageView.frame = presentedDelegate.endRectForPresentedView(indexPath)
            }) { (_) in
                presentedView.alpha = 1.0
                imageView.removeFromSuperview()
                transitionContext.containerView()?.backgroundColor = UIColor.clearColor()
                transitionContext.completeTransition(true)
        }
    }
    func animateForDismissView(transitionContext:UIViewControllerContextTransitioning) {
//        nil值校验
        guard let dismissDelegate = dismissDelegate , presentedDelegate = presentedDelegate else{
            return
        }
//        取出将要消失的View
        guard let dismissView = transitionContext.viewForKey(UITransitionContextFromViewKey)else{
            return
        }
        let imageView = dismissDelegate.imageViewForDismissView()
        let indexPath = dismissDelegate.indexPathForDismissView()
        transitionContext.containerView()?.addSubview(imageView)
        dismissView.removeFromSuperview()
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            imageView.frame = presentedDelegate.startRectForPresentedView(indexPath)
            }) { (_) in
                imageView.removeFromSuperview()
                transitionContext.completeTransition(true)
        }
    }
}