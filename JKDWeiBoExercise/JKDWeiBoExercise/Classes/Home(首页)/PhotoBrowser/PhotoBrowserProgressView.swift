//
//  PhotoBrowserProgressView.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/24.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class PhotoBrowserProgressView: UIView {
    var progress : CGFloat = 0 {
        didSet{
            setNeedsDisplay()
        }
    }
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        let center = CGPoint(x: rect.width * 0.5, y: rect.height * 0.5)
        let radius = rect.width * 0.5 - 6
        let starAngle = CGFloat(-M_PI * 0.5)
        let endAngle = CGFloat(2 * M_PI) * progress + starAngle
        let endAngle2 = CGFloat(2 * M_PI) + starAngle
        let bezierPath = UIBezierPath(arcCenter: center, radius: radius, startAngle: starAngle, endAngle: endAngle, clockwise: true)
        let bezierPath2 = UIBezierPath(arcCenter: center, radius: radius + 3, startAngle: starAngle, endAngle: endAngle2, clockwise: true)
        UIColor(white: 0.9, alpha: 0.5).setStroke()
        UIColor(white: 0.9, alpha: 0.5).setFill()
        bezierPath2.lineWidth = 2
        bezierPath.addLineToPoint(center)
        bezierPath.closePath()
        bezierPath.fill()
//        bezierPath2.closePath()
        bezierPath2.stroke()
    }
}
