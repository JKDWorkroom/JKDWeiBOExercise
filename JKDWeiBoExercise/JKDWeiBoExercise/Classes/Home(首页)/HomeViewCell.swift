//
//  HomeViewCell.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/6/2.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit
import HYLabel
class HomeViewCell: UITableViewCell {
//    约束属性
    @IBOutlet weak var retweetedLabelTopCons: NSLayoutConstraint!
    @IBOutlet weak var contentLabelWidthCons: NSLayoutConstraint!
    @IBOutlet weak var picCollectionViewTopCons: NSLayoutConstraint!
//    控件属性
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var verifiedImageView: UIImageView!
    @IBOutlet weak var screenNameLabel: UILabel!
    @IBOutlet weak var vipImageView: UIImageView!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var sourceLabel: UILabel!
    @IBOutlet weak var contentLabel: HYLabel!
    @IBOutlet weak var bottomToolView: UIView!
    @IBOutlet weak var picCollectionView: PicCollectionView!
    @IBOutlet weak var retweetedContentLabel: HYLabel!
    @IBOutlet weak var retweetedBgView: UIView!
    var statusViewModel : StatusViewModel?{
        didSet{
            guard let statusViewModel = statusViewModel else{
                return
            }
     let retweeted_test = statusViewModel.status?.retweeted_status?.text ?? ""
//            MARK:- 属性赋值
            iconImageView.sd_setImageWithURL(NSURL(string: (statusViewModel.status?.user?.profile_image_url)!))
            iconImageView.layer.cornerRadius = 20
            iconImageView.layer.masksToBounds = true
            verifiedImageView.image = statusViewModel.verifiedImage
            screenNameLabel.text = statusViewModel.status?.user?.screen_name
            vipImageView.image = statusViewModel.vipImage
            timeLabel.text = statusViewModel.createAtString
            sourceLabel.text = statusViewModel.sourceText
//             微博正文
            contentLabel.attributedText = FindEmoticon.shareIntance.FindEmoticonWithString((statusViewModel.status?.text)!, fontHeight: contentLabel.font.lineHeight)
            picCollectionView.picURLs = statusViewModel.picURLs
//            转发微博属性赋值
            if statusViewModel.status?.retweeted_status != nil {
//                1.设置转发微博正文
                if let screenName = statusViewModel.status?.user?.screen_name{
                    let retweetedText = "@" + screenName + ":" + retweeted_test
                    retweetedContentLabel.attributedText = FindEmoticon.shareIntance.FindEmoticonWithString(retweetedText, fontHeight: retweetedContentLabel.font.lineHeight)
                }else{
                    retweetedContentLabel.text = retweeted_test
                }
//                2.设置转发微博的Label距离顶部的约束
                retweetedLabelTopCons.constant = 15
//                3.设置转发背景显示
                retweetedBgView.hidden = false
//                print(statusViewModel.status?.retweeted_status?.pic_urls)
            }else{
                retweetedContentLabel.text = nil
                retweetedBgView.hidden = true
                retweetedLabelTopCons.constant = 0
            }
            picCollectionViewTopCons.constant = statusViewModel.picURLs.count == 0 ? 0 : 10
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        contentLabelWidthCons.constant = UIScreen.mainScreen().bounds.width - 2 * edgeMargin
//       MARK:- 监听微博正文中，特殊字符串的点击
//        @了谁
        contentLabel.userTapHandler = {(label,user,rang) in
            print(label)
            print(user)
            print(rang)
        }
//        话题
        contentLabel.topicTapHandler = {(label,topic,rang) in
            print(topic)
            print(label)
            print(rang)
        }
//        url
        contentLabel.linkTapHandler = {(label,link,rang) in
            print(label)
            print(link)
            print(rang)
        }
    }
}
extension HomeViewCell{
    func cellHeight(statusViewModel : StatusViewModel) -> CGFloat {
        //        给模型赋值
        self.statusViewModel = statusViewModel
        //        更新所有约束
        self.layoutIfNeeded()
        return CGRectGetMaxY(bottomToolView.frame)
    }
    
}
