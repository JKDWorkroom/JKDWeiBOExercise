//
//  PopoverAnimator.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/12.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class PopoverAnimator: NSObject {
    var isPresented = true
    var animateCellBack : ((isPresented:Bool) -> ())?
    var presentFrame : CGRect = CGRectZero
}
extension PopoverAnimator:UIViewControllerTransitioningDelegate{
    // 目的:改变弹出View的尺寸和添加蒙版
    func presentationControllerForPresentedViewController(presented: UIViewController, presentingViewController presenting: UIViewController, sourceViewController source: UIViewController) -> UIPresentationController? {
        let present = JKDPresentationController(presentedViewController: presented, presentingViewController: presenting)
        present.presentedViewFrame = presentFrame
        return present
    }
    //   目的:弹出动画交给谁来管理
    func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = true
        if let animateCellBack = animateCellBack{
            animateCellBack(isPresented:isPresented)
     }
        return self
    }
    
    //      目的:消失动画交给谁来管理
    func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        isPresented = false
        if let animateCellBack = animateCellBack{
            animateCellBack(isPresented:isPresented)
        }
        return self
    }

}
extension PopoverAnimator : UIViewControllerAnimatedTransitioning{
    func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
        return 0.5
    }
    // 在该方法中有一个参数transitionContext
    // 通过transitionContext,可以获取到弹出的View,也可以获取到消失的View
    func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
        isPresented ? presentedAnimate(transitionContext):dismissAnimate(transitionContext)
        
        }
    func presentedAnimate(transitionContext: UIViewControllerContextTransitioning){
        // UITransitionContextToViewKey : 获取弹出的View
        let presentedView = transitionContext.viewForKey(UITransitionContextToViewKey)
        transitionContext.containerView()?.addSubview(presentedView!)
        presentedView!.transform = CGAffineTransformMakeScale(1.0, 0.0)
        presentedView!.layer.anchorPoint = CGPoint(x: 0.5, y: 0)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            presentedView!.transform = CGAffineTransformIdentity
        }) { (_) in
            transitionContext.completeTransition(true)
        }
    }
    func dismissAnimate(transitionContext: UIViewControllerContextTransitioning){
        // UITransitionContextFromViewKey : 获取消失的View
        let dismissView = transitionContext.viewForKey(UITransitionContextFromViewKey)
        UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
            dismissView?.transform = CGAffineTransformMakeScale(1, 0.000000001)
        }) { (_) in
            dismissView?.removeFromSuperview()
            transitionContext.completeTransition(true)
            
        }

    }

}