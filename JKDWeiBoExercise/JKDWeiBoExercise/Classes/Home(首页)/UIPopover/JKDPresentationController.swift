//
//  JKDPresentationController.swift
//  JKDWeiBoExercise
//
//  Created by 简康达 on 16/5/11.
//  Copyright © 2016年 JKD. All rights reserved.
//

import UIKit

class JKDPresentationController: UIPresentationController {
    lazy var coverView = UIView()
    var presentedViewFrame : CGRect = CGRectZero
    override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()
        presentedView()?.frame = presentedViewFrame
//        添加蒙板
        setupCoverView()
    }
    

}
extension JKDPresentationController{
//    设置蒙板
    func setupCoverView(){
        coverView.frame = (containerView?.bounds)!
        coverView.backgroundColor = UIColor(white: 0.8, alpha: 0.2)
//        添加手势，监控点击
        let tapGes = UITapGestureRecognizer(target: self, action: #selector(JKDPresentationController.coverViewClick))
        coverView.addGestureRecognizer(tapGes)
//        将蒙板添加到Popover之下
        containerView?.insertSubview(coverView, belowSubview: presentedView()!)
    }
    @objc func coverViewClick(){
        presentedViewController.dismissViewControllerAnimated(true, completion: nil)
    }
}
